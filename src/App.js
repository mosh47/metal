import React from 'react';


export default class App extends React.Component {

    constructor(props) {
      super(props);

      this.state = {
        response: null,
      }

    }

    componentDidMount() {

      const socket = new WebSocket("ws://localhost:8080")

      socket.onmessage = (x) => {
        this.setState({ response: x.data })
      }

    }

    render() {

        return (
            <React.Fragment>
                This is working somewhat { this.state.response }, WORK!
            </React.Fragment>

        );
    }

}
