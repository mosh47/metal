import asyncio
import websockets
import time

async def start(ws, path):
    while True:
        await ws.send(str(int(time.time())))
        print('Sending: ', int(time.time()*1000))
        await asyncio.sleep(0.6)


loop = asyncio.get_event_loop()
loop.run_until_complete(websockets.serve(start, 'localhost', 8080))
loop.run_forever()
